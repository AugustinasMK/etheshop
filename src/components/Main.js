import React, {Component} from 'react';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {addProductShow: false};
        this.state = {buyProductShow: false};
        // This binding is necessary to make `this` work in the callback
        this.showAddProduct = this.showAddProduct.bind(this);
        this.showBuyProduct = this.showBuyProduct.bind(this);
    }

    showAddProduct() {
        this.setState(state => ({
            addProductShow: !state.addProductShow
        }));
    }

    showBuyProduct() {
        this.setState(state => ({
            buyProductShow: !state.buyProductShow
        }));
    }

    render() {
        return (
            <div id="content">
                <div id="ProductsDiv">
                    <h1 onClick={this.showAddProduct}>Products</h1>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Owner</th>
                            <th scope="col"/>
                        </tr>
                        </thead>
                        <tbody id="productList">
                        {this.props.products.map((product, key) => {
                            return (
                                <tr key={key}>
                                    <th scope="row">{product.id.toString()}</th>
                                    <td>{product.name}</td>
                                    <td>{product.price.toString()} Eth</td>
                                    <td>{product.quantity.toString()}</td>
                                    <td>{product.seller}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                    <h2 onClick={this.showAddProduct}>Add Product</h2>
                    {this.state.addProductShow && <div id="addProductDiv">
                        <form onSubmit={(event) => {
                            event.preventDefault();
                            const name = this.productName.value;
                            const price = this.productPrice.value.toString();
                            const quantity = this.productQuantity.value;
                            this.props.createProduct(name, price, quantity);
                            this.setState({addProductShow: false});
                        }}>
                            <div className="form-group mr-sm-2">
                                <input
                                    id="productName"
                                    type="text"
                                    ref={(input) => {
                                        this.productName = input
                                    }}
                                    className="form-control"
                                    placeholder="Product Name"
                                    required/>
                            </div>
                            <div className="form-group mr-sm-2">
                                <input
                                    id="productPrice"
                                    type="text"
                                    ref={(input) => {
                                        this.productPrice = input
                                    }}
                                    className="form-control"
                                    placeholder="Product Price"
                                    required/>
                            </div>
                            <div className="form-group mr-sm-2">
                                <input
                                    id="productQuant"
                                    type="text"
                                    ref={(input) => {
                                        this.productQuantity = input
                                    }}
                                    className="form-control"
                                    placeholder="Product Quantity"
                                    required/>
                            </div>
                            <button type="submit" className="btn btn-primary">Add Product</button>
                        </form>
                    </div>}
                    <p/>
                    <h2 onClick={this.showBuyProduct}>Buy Product</h2>
                    {this.state.buyProductShow && <div id="buyProductDiv">
                        <form onSubmit={(event) => {
                            event.preventDefault();
                            const id = this.orderProductId.value;
                            const quantity = this.orderQuantity.value;
                            const shipAddress = this.shipAddress.value.toString();
                            this.props.buyProduct(id, quantity, shipAddress);
                            this.setState({buyProductShow: false});
                        }}>
                            <div className="form-group mr-sm-2">
                                <input
                                    id="orderProductId"
                                    type="text"
                                    ref={(input) => {
                                        this.orderProductId = input
                                    }}
                                    className="form-control"
                                    placeholder="Product ID"
                                    required/>
                            </div>
                            <div className="form-group mr-sm-2">
                                <input
                                    id="orderQuantity"
                                    type="text"
                                    ref={(input) => {
                                        this.orderQuantity = input
                                    }}
                                    className="form-control"
                                    placeholder="Product Quantity"
                                    required/>
                            </div>
                            <div className="form-group mr-sm-2">
                                <input
                                    id="shipAddress"
                                    type="text"
                                    ref={(input) => {
                                        this.shipAddress = input
                                    }}
                                    className="form-control"
                                    placeholder="Shipment Address"
                                    required/>
                            </div>
                            <button type="submit" className="btn btn-primary">Buy Product</button>
                        </form>
                    </div>}
                </div>
                <br/><br/>
                <div id="OrdersDiv">
                    <h1>Unpaid Orders</h1>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col"/>
                        </tr>
                        </thead>
                        <tbody id="uorderList">
                        {this.props.uorders.map((uorder, key) => {
                            return (
                                <tr key={key}>
                                    <th scope="row">{uorder.id.toString()}</th>
                                    <td>{uorder.productId.toString()}</td>
                                    <td>{uorder.price.toString()} Eth + 0.1 Eth Shipment</td>
                                    <td>{uorder.quantity.toString()}</td>
                                    <td>
                                        {!uorder.paid
                                            ? <button
                                                id={uorder.id}
                                                value={(uorder.price).toString() + '.1'}
                                                onClick={(event) => {
                                                    this.props.sendSafepay(event.target.id, event.target.value)
                                                }}
                                            >
                                                Pay for order
                                            </button>
                                            : null
                                        }
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                    <h1>Undelivered Orders</h1>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col"/>
                        </tr>
                        </thead>
                        <tbody id="uorderList">
                        {this.props.dorders.map((uorder, key) => {
                            return (
                                <tr key={key}>
                                    <th scope="row">{uorder.id.toString()}</th>
                                    <td>{uorder.productId.toString()}</td>
                                    <td>{uorder.price.toString()} Eth + 0.1 Eth Shipment</td>
                                    <td>{uorder.quantity.toString()}</td>
                                    <td>
                                        {uorder.paid && !uorder.closed
                                            ? <button
                                                id={uorder.id}
                                                onClick={(event) => {
                                                    this.props.receiveOrder(event.target.id)
                                                }}
                                            >
                                                Receive order
                                            </button>
                                            : null
                                        }
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                </div>
                <br/><br/>
                <div id="ShipmentDiv">
                    <h1>Shipments</h1>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Order</th>
                            <th scope="col">Price</th>
                            <th scope="col">Buyer Address</th>
                            <th scope="col"/>
                        </tr>
                        </thead>
                        <tbody id="shipmentList">
                        {this.props.shipments.map((shipment, key) => {
                            return (
                                <tr key={key}>
                                    <th scope="row">{shipment.id.toString()}</th>
                                    <td>{shipment.orderID.toString()}</td>
                                    <td>{window.web3.utils.fromWei(shipment.price.toString(), 'Ether')} Eth</td>
                                    <td>{shipment.adresas}</td>
                                    <td>
                                        {(!shipment.delivered) && shipment.paid && !shipment.taken
                                            ? <button
                                                name={shipment.orderID}
                                                value={shipment.adresas}
                                                onClick={(event) => {
                                                    this.props.shipProduct(event.target.name, event.target.value)
                                                }}
                                            >
                                                Take Shipment
                                            </button>
                                            : null
                                        }
                                    </td>
                                </tr>
                            )
                        })}
                        {/*{this.props.shipments.map((shipment, key) => {
                            return (
                                <tr key={key}>
                                    <th scope="row">{shipment.id.toString()}</th>
                                    <td>{shipment.orderID}</td>
                                    <td>{shipment.price.toString()} Eth</td>
                                    <td>{shipment.adresas}</td>
                                    <td>
                                        {!shipment.delivered
                                            ? <button
                                                id={shipment.orderID}
                                                onClick={(event) => {
                                                    this.props.shipProduct(event.target.id)
                                                }}
                                            >
                                                Take Shipment
                                            </button>
                                            : null
                                        }
                                    </td>
                                </tr>
                            )
                        })}*/}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Main;