import React, {Component} from 'react';
import logo from '../logo.png';
import './App.css';
import Navbar from './Navbar'
import Main from './Main'
import Market from '../abis/Market.json'
import Web3 from 'web3'

class App extends Component {

    async componentWillMount() {
        await this.loadWeb3();
        await this.loadBlockchainData();
    }

    async loadWeb3() {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            await window.ethereum.enable()
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
        }
    }

    async loadBlockchainData() {
        const web3 = window.web3;
        const accounts = await web3.eth.getAccounts();
        console.log(accounts);
        this.setState({account: accounts[0]});
        const networkId = await web3.eth.net.getId();
        const networkData = Market.networks[networkId];
        if (networkData) {
            let i;
            const market = web3.eth.Contract(Market.abi, networkData.address);
            this.setState({market});
            const productCount = await market.methods.productCount().call();
            this.setState({ productCount });
            for (i = 1; i <= productCount; i++) {
                const product = await market.methods.products(i).call();
                this.setState({
                    products: [...this.state.products, product]
                })
            }
            const orderCount = await market.methods.orderCount().call();
            this.setState({ orderCount });
            for (i = 1; i <= orderCount; i++) {
                const order = await market.methods.orders(i).call();
                if (order.buyer === this.state.account && !order.paid) {
                    this.setState({
                        orders: [...this.state.orders, order]
                    })
                }
            }
            for (i = 1; i <= orderCount; i++) {
                const order = await market.methods.orders(i).call();
                if (order.buyer === this.state.account && order.paid && !order.closed) {
                    this.setState({
                        dorders: [...this.state.dorders, order]
                    })
                }
            }
            const shipmentCount = await market.methods.shipmentCount().call();
            this.setState({ shipmentCount });
            for (i = 1; i <= shipmentCount; i++) {
                const shipment = await market.methods.shipments(i).call();
                if (shipment.delivered === false) {
                    this.setState({
                        shipments: [...this.state.shipments, shipment]
                    })
                }
            }
            console.log(this.state.products);
            console.log(this.state.orders);
            console.log(this.state.shipments);
            this.setState({loading: false})
        } else {
            window.alert('Marketplace contract not deployed to detected network.')
        }
    }

    createProduct(name, price, quantity) {
        this.setState({loading: true});
        this.state.market.methods.createProduct(name, price, quantity).send({from: this.state.account});
    }

    buyProduct(id, quantity, priAdresas) {
        this.setState({loading: true});
        this.state.market.methods.buyProduct(id, quantity, priAdresas).send({from: this.state.account});
    }

    sendSafepay(id, price) {
        this.setState({loading: true});
        this.state.market.methods.sendSafepay(id).send({from: this.state.account, value: window.web3.utils.toWei(price, 'Ether')});
    }

    shipProduct(id) {
        this.setState({loading: true});
        this.state.market.methods.shipProduct(id).send({from: this.state.account});
    }

    receiveOrder(id) {
        this.setState({loading: true});
        this.state.market.methods.receiveOrder(id).send({from: this.state.account});
    }

    constructor(props) {
        super(props);
        this.state = {
            account: '',
            productCount: 0,
            products: [],
            orders: [],
            dorders: [],
            shipments: [],
            loading: true
        };
        this.createProduct = this.createProduct.bind(this);
        this.buyProduct = this.buyProduct.bind(this);
        this.sendSafepay = this.sendSafepay.bind(this);
        this.shipProduct = this.shipProduct.bind(this);
        this.receiveOrder = this.receiveOrder.bind(this);
    }

    render() {
        return (
            <div>
                <Navbar account={this.state.account}/>
                <div className="container-fluid mt-5">
                    <div className="row">
                        <main role="main" className="col-lg-12 d-flex">
                            {this.state.loading
                                ? <div id="loader" className="text-center"><p className="text-center">Loading...</p></div>
                                : <Main
                                    products={this.state.products}
                                    uorders={this.state.orders}
                                    dorders={this.state.dorders}
                                    shipments={this.state.shipments}
                                    createProduct={this.createProduct}
                                    buyProduct={this.buyProduct}
                                    shipProduct={this.shipProduct}
                                    sendSafepay={this.sendSafepay}
                                    receiveOrder={this.receiveOrder}/>
                            }
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
