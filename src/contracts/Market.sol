pragma solidity ^0.5.0;

contract Market {
    string public name;
    uint public productCount;
    uint public orderCount;
    uint public shipmentCount;
    mapping(uint => Product) public products;
    mapping(uint => Order) public orders;
    mapping(uint => Shipment) public shipments;

    struct Product {
        uint id;
        string name;
        uint quantity;
        uint price;
        address payable seller;
    }

    struct Shipment {
        uint id;
        uint orderID;
        string adresas;
        address payable courier;
        uint price;
        uint safepay;
        bool delivered;
        bool paid;
        bool taken;
    }

    struct Order {
        uint id;
        address buyer;
        uint productId;
        uint quantity;
        uint price;
        uint safepay;
        uint shipmentId;
        bool paid;
        bool closed;
    }

    event ProductCreated(uint id, string name, uint quantity, uint price, address payable seller);
    event ProductBought(uint id, string name, uint quantity, uint price, address payable buyer);

    event OrderSent(uint orderno, address courier);
    event SafepaySent(address buyer, uint orderno, uint value, uint now, uint safepay, address courier);
    event OrderDelivered(address buyer, uint orderno, uint real_delivey_date);


    constructor() public {
        name = "ISIS";
        productCount = 0;
        orderCount = 0;
        shipmentCount = 0;
    }

    function createProduct(string memory _name, uint _price, uint _quant) public {
        require(bytes(_name).length > 0);
        require(_price > 0);
        require(_quant > 0);
        productCount++;
        products[productCount] = Product(productCount, _name, _quant, _price, msg.sender);
        emit ProductCreated(productCount, _name, _quant, _price, msg.sender);
    }

    function buyProduct(uint _id, uint _quant, string memory pristatymoAdresas) public {
        require(_id > 0 && _id <= productCount);
        Product memory product = products[_id];
        require(_quant <= product.quantity);
        product.quantity -= _quant;
        products[_id] = product;
        orderCount++;
        uint total = product.price * _quant;
        shipmentCount++;
        Shipment memory ship = Shipment(shipmentCount,orderCount, pristatymoAdresas, address(0), 0.1 ether, 0, false, false, false);
        shipments[shipmentCount] = ship;
        orders[orderCount] = Order(orderCount, msg.sender, _id, _quant, total, 0, shipmentCount, false, false);

        emit ProductBought(orderCount, product.name, _quant, total, msg.sender);
    }

    function sendSafepay(uint _id) public payable {
        Order memory order = orders[_id];
        Shipment memory ship = shipments[order.shipmentId];
        require(order.buyer == msg.sender);
        order.safepay = msg.value;
        ship.safepay = 1;
        order.paid = true;
        ship.paid = true;
        orders[_id] = order;
        shipments[order.shipmentId] = ship;
        emit SafepaySent(msg.sender, _id, msg.value, now, order.safepay, ship.courier);
    }

    function shipProduct(uint _id) public {
        Order memory order = orders[_id];
        Shipment memory ship = shipments[order.shipmentId];
        require(!ship.delivered && ship.courier == address(0));
        ship.courier = msg.sender;
        ship.taken = true;
        shipments[order.shipmentId] = ship;
        emit OrderSent(_id, ship.courier);
    }

    function receiveOrder(uint _id) public {
        Order memory order = orders[_id];
        Product memory pro = products[order.productId];
        Shipment memory ship = shipments[order.shipmentId];
        address payable _seller = pro.seller;
        address payable _courier = ship.courier;
        require(order.buyer == msg.sender);
        require(!order.closed);
        address(_seller).transfer(order.price);
        address(_courier).transfer(0.1 ether);
        order.closed = true;
        ship.delivered = true;
        orders[_id] = order;
        shipments[order.shipmentId] = ship;
        emit OrderDelivered(msg.sender, _id, now);
    }
}
