"use strict";
const Market = artifacts.require('./Market.sol');

require('chai').use(require('chai-as-promised')).should();

contract('Market', ([deployer, seller, buyer, courier]) => {
    let market;

    before(async () => {
        market = await Market.deployed()
    });

    describe('deployment', async () => {
        it('Deploys successfully', async () => {
            const address = await market.address;
            assert.notEqual(address, 0x0);
            assert.notEqual(address, '');
            assert.notEqual(address, null);
            assert.notEqual(address, undefined)
        });

        it('Has a name', async () => {
            const name = await market.name();
            assert.equal(name, 'ISIS');
        })
    });

    describe('Products', async () => {
        let result, productCount;

        before(async () => {
            result = await market.createProduct('NovaGrappler VR', web3.utils.toWei('0.1', 'Ether'), 4, {from: seller});
            productCount = await market.productCount();
        });

        it('Creates product', async () => {
            assert.equal(productCount.toNumber(), 1);
            const event = result.logs[0].args;
            assert.equal(event.id.toNumber(), productCount.toNumber(), 'ID sutinka');
            assert.equal(event.name, 'NovaGrappler VR', 'Vardai sutinka');
            assert.equal(event.price, web3.utils.toWei('0.1', 'Ether'), 'Kaina sutinka');
            assert.equal(event.seller, seller, 'ID sutinka');

            // The Bad
            await market.createProduct('', web3.utils.toWei('0.1', 'Ether'), 4, {from: seller}).should.be.rejected;
            await market.createProduct('a', web3.utils.toWei('0', 'Ether'), 4, {from: seller}).should.be.rejected;
            await market.createProduct('a', web3.utils.toWei('0.1', 'Ether'), 0, {from: seller}).should.be.rejected;
        })
    });

    describe('Orders', async () =>{
        let result, orderCount;
        before (async () =>{
            await market.createProduct('NovaGrappler VR', web3.utils.toWei('0.1', 'Ether'), 4, {from: seller});
            orderCount = await market.orderCount();
            result = await market.buyProduct(1, 2, 'Antakalnio g. 43 - 14, Vilnius', {from: buyer});
            orderCount = await market.orderCount();
        });

        it('Creates Order', async () => {
            assert.equal(orderCount.toNumber(), 1);
            const event = result.logs[0].args;
            assert.equal(event.id.toNumber(), orderCount.toNumber(), 'ID sutinka');
            assert.equal(event.buyer, buyer, 'Adresai sutinka');
            assert.equal(event.quantity, 2, 'Kiekis sutinka');
            assert.equal(event.price.toString(), web3.utils.toWei('0.2', 'Ether'), 'Kaina sutinka');
            assert(!event.closed);

            // The Bad
            await market.buyProduct(99, 2, {from: buyer}).should.be.rejected;
            await market.buyProduct(1, 99, {from: buyer}).should.be.rejected;
        });

        it('Ship Order', async () => {
            assert.equal(orderCount.toNumber(), 1);
            result = await market.shipProduct(orderCount, {from: courier});
            const event = result.logs[0].args;
            assert.equal(event.courier, courier, 'Courier address');
        });

        it('SafePay for Order', async () => {
            assert.equal(orderCount.toNumber(), 1);
            result = await market.sendSafepay(orderCount, {from: buyer, value: web3.utils.toWei('0.3', 'Ether')});
            const event = result.logs[0].args;
            assert.equal(event.safepay.toString(), web3.utils.toWei('0.3', 'Ether').toString(), 'Safepay atitinka');
        });

        it('Receive Order', async () => {
            let ob1 = await web3.eth.getBalance(seller);
            ob1 = new web3.utils.BN(ob1);

            let ob2 = await web3.eth.getBalance(courier);
            ob2 = new web3.utils.BN(ob2);

            assert.equal(orderCount.toNumber(), 1);
            result = await market.receiveOrder(orderCount, {from: buyer});

            let nb1 = await web3.eth.getBalance(seller);
            nb1 = new web3.utils.BN(nb1);
            let price1 = web3.utils.toWei('0.2', 'Ether');
            price1 = new web3.utils.BN(price1);
            assert.equal(nb1.toString(), (ob1.add(price1)).toString());

            let nb2 = await web3.eth.getBalance(courier);
            nb2 = new web3.utils.BN(nb2);
            let price2 = web3.utils.toWei('0.1', 'Ether');
            price2 = new web3.utils.BN(price2);
            assert.equal(nb2.toString(), (ob2.add(price2)).toString());

        });
    });
});
