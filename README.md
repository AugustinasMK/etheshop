# ETHeShop
## Reikalavimai
1. Aprašykite išmaniosios sutarties verslo modelio logiką, kurią įgyvendins išmanioji sutartis. 
2. Realizuokite pirmąjame žingsnyje aprašytą verslo logiką išmanioje sutartyje **Solidyti** kalboje.
3. Sukurkite decentralizuotos aplikacijos `Front-End`ą (tinklapį arba mobiliąją aplikaciją), kuri įgalintų bendravimą su išmaniąja sutartimi. 

## Programa
Parašyta naudojantis React, Web3j, Truffle Suite. Testuota lokialame tinkle naudojantis Ganache aplinka. Naudotas WebStorm IDE. Programoje yra keli vartotojai: pirkėjas, pardavėjas kurjeris.

## Changelog
### v1.0 - 2019-12-20
#### Pridėta
Įvykdyti reikalavimai
